# USC Gitlab Example

Set `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` as protected variables in `settings/ci_cd/variables`.

```
image:
  name: ghcr.io/mammutmw/usc
  entrypoint: [ "" ]

upload:
  script:
    - /bin/usc upload --target www.cte.ikeadt.com/aj/xx/test/gitlab --src README.md
```
